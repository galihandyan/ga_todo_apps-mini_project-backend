const models = require('../models');

module.exports = (modelName) => {
  const Model = models[modelName];

  return async function(req, res, next) {
    try {
      let instance = await Model.findByPk(req.params.taskId);

      if (instance.users_id !== req.user.id){
        res.status(403).json({
          status:'fail',
          message: "You don't have permission"
        })
      }
      next();
    }

    catch(err) {
      res.status(400).json({
        status:'fail',
        message: err
      })
    }
  }
}