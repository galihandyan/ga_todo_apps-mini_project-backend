'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profiles = sequelize.define('Profiles', {
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    image: {
      type: DataTypes.TEXT,
      defaultValue: 'https://ik.imagekit.io/g4nt4prim4ry/sample/BlueHead_JkxqLVWc7.jpg'
    },
    users_id: DataTypes.INTEGER
  }, {});
  Profiles.associate = function (models) {
    // associations can be defined here
    Profiles.belongsTo(models.Users, {
      foreignKey: 'users_id'
    })
  };
  return Profiles;
};