'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    password: DataTypes.STRING
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },

      beforeCreate: instance => {
        instance.email = instance.email.toLowerCase();
        instance.password = bcrypt.hashSync(instance.password, 10);
      }
    }
  });
  Users.associate = function (models) {
    // associations can be defined here
    Users.hasMany(models.Tasks, {
        foreignKey: 'users_id'
      }),
      Users.hasOne(models.Profiles, {
        foreignKey: 'users_id'
      })
  };
  return Users;
};