'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tasks = sequelize.define('Tasks', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    completed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    importance: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    due_date: {
      type: DataTypes.DATE,
      validate: {
        isDate: true
      }
    },
    users_id: DataTypes.INTEGER
  }, {});
  Tasks.associate = function (models) {
    // associations can be defined here
    Tasks.belongsTo(models.Users, {
      foreignKey: 'users_id'
    })
  };
  return Tasks;
};