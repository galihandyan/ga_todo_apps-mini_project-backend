const request = require('supertest');
const app = require('../../server');
const db = require('../../models');
const {
    Profiles,
    Users
} = require('../../models');
const jwt = require('../../helper/token');
let token;

describe('Test User API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
        Users.create({
            email: 'Tester@email.com',
            password: '12345'
        }).then(user => {
            Profiles.create({
                name: "Tester",
                users_id: user.dataValues.id
            }).then(prof => {
                jwt(user.dataValues.id, prof.dataValues.name, user.dataValues.email, prof.dataValues.image)
                    .then(toks => {
                        token = toks;
                        // console.log(token+'==================');
                        done();
                    })
            })
        })
    })
    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    })

    describe('GET /api/v1/user/profile', () => {
        test('Success show profile. Status code 200', done => {
            request(app)
                .get('/api/v1/user/profile')
                .set('auth', token)
                .set('Content-Type', 'application/json')
                .then(res => {
                    console.log(token + "==================== show profile");
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('PUT /api/v1/user/update', () => {
        test('Success updated profile. Status code 201', done => {
            request(app)
                .put('/api/v1/user/profile')
                .set('auth', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/sample_image/BlueHead.jpg')
                .field({
                    name: "Task Tester Terbaru"
                })
                .then(res => {
                    console.log(token + "==================== success update");
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed updated profile. Status code 201', done => {
            request(app)
                .put('/api/v1/user/profile')
                .set('auth', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/sample_image/BlueHead.jpg')
                .field({
                    name: ""
                })
                .then(res => {
                    console.log(token + "==================== fail update");
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('POST /api/v1/user/register', () => {
        test('Success register. Status code 201', done => {
            request(app)
                .post('/api/v1/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'another user tester',
                    email: 'another@gmail.com',
                    password: 'another'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed register. Status code 400', done => {
            request(app)
                .post('/api/v1/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'another user tester',
                    email: 'another@gmail.com',
                    password: null
                })
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('POST /api/v1/user/login', () => {
        test('Success login. Status code 201', done => {
            request(app)
                .post('/api/v1/user/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'Tester@email.com',
                    password: '12345'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed login. Status code 400', done => {
            request(app)
                .post('/api/v1/user/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'Tester@email.com',
                    password: '123'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})