const request = require('supertest');
const app = require('../../server');
const db = require('../../models');
const {
    Profiles,
    Users
} = require('../../models');
const jwt = require('../../helper/token');
let token;

describe('Task API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
        Users.create({
            email: 'Tester@email.com',
            password: '12345'
        }).then(user => {
            Profiles.create({
                name: "Tester",
                users_id: user.dataValues.id
            }).then(prof => {
                jwt(user.dataValues.id, prof.dataValues.name, user.dataValues.email, prof.dataValues.image)
                    .then(toks => {
                        token = toks;
                        done();
                    })
            })
        })

    })

    afterAll(()=>{
        db.sequelize.query('TRUNCATE "Users", "Tasks", "Profiles" RESTART IDENTITY');
    })
    
    describe('POST /api/v1/task/create', () => {
        test('Success Create Task. Status code 201', done => {
            request(app)
                .post('/api/v1/task/create')
                .set('Content-Type', 'application/json')
                .set('auth', token)
                .send({
                    name: 'Test Task',
                    description: 'desc test task',
                    due_date: '2020-08-15'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed Create Task. Status code 422', done => {
            request(app)
                .post('/api/v1/task/create')
                .set('auth', token)
                .set('Content-Type', 'application/json')
                .send({
                    name: 'Test Task',
                    description: 'desc test task',
                    due_date: null
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('GET /api/v1/task/get', () => {
        test('Success Show All Task. Status code 200', done => {
            request(app)
                .get('/api/v1/task')
                .set('auth', token)
                .set('Content-Type', 'application/json')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Success Show All Importance Task. Status code 200', done => {
            request(app)
                .get('/api/v1/task/importance')
                .set('auth', token)
                .set('Content-Type', 'application/json')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Success Show All Completed Task. Status code 200', done => {
            request(app)
                .get('/api/v1/task/completed')
                .set('auth', token)
                .set('Content-Type', 'application/json')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('PUT /api/v1/task/:taskId', () => {
        test('Success Update Task. Status code 201', done => {
            request(app)
                .put('/api/v1/task/1')
                .set('Content-Type', 'application/json')
                .set('auth', token)
                .send({
                    importance: true
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Failed Update Task. Status code 201', done => {
            request(app)
                .put('/api/v1/task/1')
                .set('Content-Type', 'application/json')
                .set('auth', token)
                .send({
                    importance: "benar"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('DELETE /api/v1/task/:taskId', () => {
        test('Success Delete Task. Status code 201', done => {
            request(app)
                .delete('/api/v1/task/1')
                .set('Content-Type', 'application/json')
                .set('auth', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
})