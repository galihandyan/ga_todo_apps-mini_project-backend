require('dotenv').config();
const express = require('express')
const app = express();
const morgan = require('morgan')
const cors = require('cors');
const router = require('./router/main-router');
const swaggerUi = require('swagger-ui-express');
const swaggerDocs = require('./swagger.json');

app.use(express.json());
app.use(cors());

if(process.env.NODE_ENV !== 'test') app.use(morgan('dev'))

app.use('/api/v1/', router)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

module.exports=app;