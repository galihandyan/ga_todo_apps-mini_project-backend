const {
  Tasks,
  Users
} = require('../models')
const respon = require('../helper/succes-handler')

class Task {
  static async create(req, res, next) {
    try {
      const task = await Tasks.create({
        name: req.body.name,
        description: req.body.description,
        due_date: req.body.due_date,
        users_id: req.user.id
      });

      const key = "Task";
      respon(res, 201, key, task);
    } catch (err) {
      res.status(422).json({
        status: "fail",
        message: err.message
      });

    }
  }

  static async show(req, res, next) {
    try {
      const task = await Tasks.findAll({
        where: {
          users_id: req.user.id
        },
        order: [
          ['due_date', 'ASC']
        ],
        limit: 10,
        include: [Users]
      });
      const key = "Task";
      respon(res, 200, key, task);
    } catch (err) {
      res.status(500).json({
        status: "fail",
        message: err.message
      });

    }
  }

  static async showById(req, res) {
    try {
      const task = await Tasks.findOne({
        where: {
          users_id: req.user.id,
          id: req.params.taskId
        },
        include: [Users]
      });
      const key = "Task";
      respon(res, 200, key, task);
    } catch (err) {
      res.status(500).json({
        status: "fail",
        message: err.message
      });

    }
  }

  static async importance(req, res, net) {
    try {
      const task = await Tasks.findAll({
        where: {
          users_id: req.user.id,
          importance: true
        },
        order: [
          ['due_date', 'ASC']
        ],
        limit: 10,
        include: [Users]
      });
      const key = "Task";
      respon(res, 200, key, task);
    } catch (err) {
      res.status(500).json({
        status: "fail",
        message: err.message
      });

    }
  }

  static async completed(req, res, next) {
    try {
      const task = await Tasks.findAll({
        where: {
          users_id: req.user.id,
          completed: true
        },
        order: [
          ['due_date', 'ASC']
        ],
        limit: 10,
        include: [Users]
      });
      const key = "Task";
      respon(res, 200, key, task);
    } catch (err) {
      res.status(500).json({
        status: "fail",
        message: err.message
      });

    }
  }

  static async update(req, res, next) {
    try {
      const task = await Tasks.update(req.body, {
        where: {
          id: req.params.taskId
        }
      })

      const key = "Message";
      const message = `Task with ID ${req.params.taskId} has been updated`;
      respon(res, 201, key, message);
    } catch (err) {
      res.status(422).json({
        status: "fail",
        message: {
          err
        }
      })
    }
  }

  static async delete(req, res, next) {
    try {
      const task = await Tasks.destroy({
        where: {
          id: req.params.taskId
        }
      })

      const key = 'message';
      const message = `Task with ID ${req.params.taskId} has been deleted`

      respon(res, 200, key, message);
    } catch (err) {
      res.status(403).json({
        status: "fail",
        message: {
          err
        }
      })
    }
  }
}

module.exports = Task;