const bcrypt = require('bcryptjs');
const {
    Users,
    Profiles
} = require('../models');
const respon = require('../helper/succes-handler');
const token = require('../helper/token');
const imagekit = require('../lib/imagekit');

class User {

    static async register(req, res, next) {
        try {
            const checkEmail = await Users.findOne({
                where: {
                    email: req.body.email
                }
            })
            if (checkEmail) throw "Email already registered. Use another one"

            const checkName = await Profiles.findOne({
                where: {
                    name: req.body.name
                }
            })
            if (checkName) throw "Name already registered. Use another one"

            const data = await Users.create(req.body);
            const profile = await Profiles.create({
                name: req.body.name,
                users_id: data.id
            });
            const tokens = await token(data.id, profile.name, data.email, profile.image)
            const key = 'token';
            respon(res, 201, key, tokens);
        } catch (err) {
            res.status(400).json({
                status: 'fail',
                message: err
            })
        }
    }


    static async login(req, res, next) {
        try {
            let data = await Users.findOne({
                where: {
                    email: req.body.email.toLowerCase()
                }
            });
            if (!data) throw "Email doesn't exist";
            let check = await bcrypt.compareSync(req.body.password, data.password);
            if (!check) throw "Password Incorrect";
            let profile = await Profiles.findOne({
                where: {
                    users_id: data.id
                }
            })
            const tokens = await token(data.id, profile.name, data.email, profile.image)
            let key = "token";
            respon(res, 201, key, tokens)
        } catch (err) {
            res.status(401).json({
                status: 'fail',
                message: err
            })
        }
    }

    static async profile(req, res, next) {
        try {
            let data = await Profiles.findOne({
                where: {
                    users_id: req.user.id
                },
                include: [Users]
            });
            let key = ['profile'];
            respon(res, 200, key, data)
        } catch (err) {
            res.status(500).json({
                status: 'fail',
                message: err
            })
        }
    }

    static async editProfile(req, res, next) {
        try {
            const image = await imagekit.upload({
                file: req.file.buffer,
                fileName: "IMG_" + Date.now()
            })
            console.log(image.url);
            await Profiles.update({
                name: req.body.name,
                image: image.url
            }, {
                where: {
                    users_id: req.user.id
                }
            })

            const key = "message";
            const data = "Profile has been updated";
            respon(res, 201, key, data);
        } catch (err) {
            res.status(400).json({
                status: "fail",
                message: 'error on controller',
                error: err.message
            })
        }
    }
}

module.exports = User;