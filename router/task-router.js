const router = require('express').Router();
const Task = require('../controller/task-controller');
const auth = require('../middleware/authenticate');
const ownership = require('../middleware/ownership');

router.get('/', auth, Task.show);
router.post('/create', auth, Task.create);
router.get('/importance', auth, Task.importance);
router.get('/completed', auth, Task.completed);
router.get('/:taskId',auth, Task.showById)
router.put('/:taskId', auth, ownership('Tasks'), Task.update);
router.delete('/:taskId', auth, ownership('Tasks'), Task.delete);

module.exports = router;