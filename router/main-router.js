const router = require('express').Router();
const routerUser = require('./user-router');
const routerTask = require('./task-router');

router.use('/user', routerUser);
router.use('/task', routerTask);

module.exports = router;