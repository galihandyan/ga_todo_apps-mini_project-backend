const router = require('express').Router();
const User = require('../controller/user-controller');
const auth = require('../middleware/authenticate');
const ownership = require('../middleware/ownership');
const upload = require('../middleware/uploader');

router.post('/register', User.register);
router.post('/login', User.login);
router.get('/profile', auth, User.profile);

//this endpoint are opotional
router.put('/profile', auth, upload.single('image'), User.editProfile);

module.exports = router;