const imagekit = require('../lib/imagekit');

module.exports = async (req, res, next) => {
    try {
        const image = imagekit.upload({
            file: req.file.buffer,
            fileName: "IMG_" + Date.now()
        })

        req.image = image;
        next();
    } catch (error) {
        res.status(400).json({
            status:'fail',
            message:'error helper imagekit',
            error: error
        })
    }
}