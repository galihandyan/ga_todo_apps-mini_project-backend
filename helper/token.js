const jwt = require('jsonwebtoken');

module.exports = async (id, name, email, image) => {
    try {
        let token = await jwt.sign({
            id: id,
            name: name,
            email: email,
            image: image
        }, process.env.SECRET_KEY)
        return token;
    } catch (error) {
        console.log(error);;
    }
}